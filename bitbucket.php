<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
/**
 * Tell the script this is an active end point.
 */
define( 'ACTIVE_DEPLOY_ENDPOINT', true );

require_once 'deploy-config.php';
/**
 * Deploys BitBucket git repos
 */
class BitBucket_Deploy extends Deploy {
	/**
	 * Decodes and validates the data from bitbucket and calls the 
	 * deploy constructor to deploy the new code.
	 *
	 * @param 	string 	$payload 	The JSON encoded payload data.
	 */
	function __construct( ) {
		$bod = file_get_contents('php://input');
		$payload = json_decode( $bod, true );
		$name = $payload['repository']['name'];
		
		foreach(parent::$repos as $repo)
		{
			if ( $repo['name'] == $name && $repo['branch'] === $payload['push']['changes'][0]['new']['name'] ) {
				$this->log( "Starting to fetch from branch: " . $payload['push']['changes'][0]['new']['name'] );
				$data = $repo;
				$data['commit'] = $payload['push']['changes'][0]['commits'][0]['hash'];
				parent::__construct( $name, $data, $payload );
				die;
			}
		}
		
		echo "[Sint] Detected Git push for branch '" . $payload['push']['changes'][0]['new']['name'] . "'. Not deploying to server<br/>";
	}
}
// Start the deploy attempt.
new BitBucket_Deploy( );
?>