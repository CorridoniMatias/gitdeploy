<?php
/**
 * The repos that we want to deploy.
 *
 * Each repos will be an entry in the array in the following way:
 * 'repo name' => array( // Required. This is the repo name
 * 		'path'   => '/path/to/local/repo/', // Required. The local path to your code.
 * 		'branch' => 'the_desired_deploy_branch', // Required. Deployment branch.
 *		'remote' => 'git_remote_repo', // Optional. Defaults to 'origin'
 * 		'post_deploy' => 'callback' // Optional callback function for whatever.
 * 		'secret' => '' // Optional. The secret specified in the webhook settings (Only works with GitHub).
 * )
 *
 * You can put as many of these together as you want, each one is simply 
 * another entry in the $repos array. To set up a deploy create a deploy key
 * for your repo on github or bitbucket. You can generate multiple deploy keys
 * for multiple repos.
 * @see https://confluence.atlassian.com/pages/viewpage.action?pageId=271943168
 *
 * Note that deploy keys are only necessary if the repo is private. If it is a
 * public repo, then you do not need a key to get read only access to the repo
 * which is really what we are after for deployment.
 *
 * Once you have done an initial git pull in the desired code location, you can
 * run 'pwd' to get the full directory of your git repo. Once done, enter that
 * full path in the 'path' option for that repo. The optional callback will allow
 * you to ping something else as well such as hitting a DB update script or any
 * other configuration you may need to do for the newly deployed code.
 */
 
 //Post deploy functions
function sintPostDeploy()
{
	exec ("find -type f | xargs chmod 644 ; find -type d | xargs chmod 755");

	if(file_put_contents("/home/botenxsl/public_html/sint.site/webapp/.htaccess",
	 "<FilesMatch \".\">\nAuthType Basic\nAuthName \"Sint Web App\"\nAuthUserFile \"/home/botenxsl/.htpasswds/public_html/sint.site/webapp/passwd\"\nrequire valid-user\n</FilesMatch>\n", FILE_APPEND | LOCK_EX) === FALSE)
	 	echo "[Sint] Error Writing password";
	 	
	 $conts = file_get_contents('/home/botenxsl/public_html/sint.site/webapp/PHP/core/init.php');
	 
	 $conts = str_replace("define('LOCAL', true);", "define('LOCAL', false);", $conts);

	if(file_put_contents("/home/botenxsl/public_html/sint.site/webapp/PHP/core/init.php", $conts, LOCK_EX) === FALSE)
		echo "[Sint] Error setting core";
			$conts = file_get_contents("/home/botenxsl/public_html/sint.site/webapp/.htaccess");
	 
	 $conts = str_replace("RewriteEngine On", "RewriteEngine On\n\nRewriteCond %{HTTPS} off\nRewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]\n\n", $conts);

	if(file_put_contents("/home/botenxsl/public_html/sint.site/webapp/.htaccess", $conts, LOCK_EX) === FALSE)
		echo "[Sint] Error HTTPS";

}

function sintPostDeployDev()
{
	exec ("find -type f | xargs chmod 644 ; find -type d | xargs chmod 755");

	if(file_put_contents("/home/botenxsl/public_html/sint.site/devsite/webapp/.htaccess",
	 "<FilesMatch \".\">\nAuthType Basic\nAuthName \"Sint Web App\"\nAuthUserFile \"/home/botenxsl/.htpasswds/public_html/sint.site/webapp/passwd\"\nrequire valid-user\n</FilesMatch>\n", FILE_APPEND | LOCK_EX) === FALSE)
	 	echo "[Sint] Error Writing password";
	 	
	 $conts = file_get_contents('/home/botenxsl/public_html/sint.site/devsite/webapp/PHP/core/init.php');
	 
	 $conts = str_replace("define('LOCAL', true);", "define('LOCAL', false);", $conts);

	if(file_put_contents("/home/botenxsl/public_html/sint.site/devsite/webapp/PHP/core/init.php", $conts, LOCK_EX) === FALSE)
		echo "[Sint] Error setting core local";
	
	$conts = str_replace("define('HOMEDIR', dirname(dirname(dirname($_SERVER[DOCUMENT_ROOT]))) );",
				 "define('HOMEDIR', dirname(dirname(dirname(dirname($_SERVER[DOCUMENT_ROOT])))) );", $conts);
				 
	if(file_put_contents("/home/botenxsl/public_html/sint.site/devsite/webapp/PHP/core/init.php", $conts, LOCK_EX) === FALSE)
		echo "[Sint] Error setting core dir";
}

//Repos array
$repos = array(
	'Sint' => array(
		'name' 	=> 'Sint',
		'branch' => 'master',
		'remote' => 'origin',
		'path' => '/home/botenxsl/public_html/sint.site',
		'secret' => '',
		'post_deploy' => 'sintPostDeploy'
	),
	'SintDev' => array(
		'name' => 'Sint',
		'branch' => 'dev',
		'remote' => 'origin',
		'path' => '/home/botenxsl/public_html/sint.site/devsite',
		'secret' => '',
		'post_deploy' => 'sintPostDeployDev'
	)
);

/**
 * Sets the deploy log directory
 */
define( 'DEPLOY_LOG_DIR', dirname( __FILE__ ) );

/* Do not edit below this line */
require_once 'inc/class.deploy.php';
